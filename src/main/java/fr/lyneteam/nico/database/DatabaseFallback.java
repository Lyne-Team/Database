package fr.lyneteam.nico.database;

import java.sql.SQLException;

public interface DatabaseFallback {
	public void success(DatabaseLink link);
	public void error(SQLException exception);
}