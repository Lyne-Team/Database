package fr.lyneteam.nico.database;

public class DatabaseConfigurationEntry {
	private final String key;
	private final Object value;
	
	public DatabaseConfigurationEntry(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	public final String getKey() {
		return this.key;
	}
	
	public final Object getValue() {
		return this.value;
	}
}