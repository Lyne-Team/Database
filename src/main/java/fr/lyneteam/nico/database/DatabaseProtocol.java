package fr.lyneteam.nico.database;

import java.io.File;

public enum DatabaseProtocol {
	MYSQL(DatabaseMysql.class, new Configuration() {
		public Database instanciate(DatabaseConfiguration configuration, DatabaseUser user) throws Exception {
			return new DatabaseMysql(user,
					configuration.getString("host"),
					configuration.getInt("port"),
					configuration.getString("database"),
					configuration.getString("username"),
					configuration.getString("password"),
					configuration.getBoolean("indexer"),
					configuration.getBoolean("synchronous"));
		}
	}),
	FILE(DatabaseFile.class, new Configuration() {
		public Database instanciate(DatabaseConfiguration configuration, DatabaseUser user) throws Exception {
			return new DatabaseFile(user,
					new File(configuration.getString("file")),
					configuration.getBoolean("indexer"),
					configuration.getBoolean("synchronous"));
		}
	}),
	FAKE(DatabaseFake.class, new Configuration() {
		public Database instanciate(DatabaseConfiguration configuration, DatabaseUser user) throws Exception {
			return new DatabaseFake(user);
		}
	});
	
	private interface Configuration {
		public Database instanciate(DatabaseConfiguration configuration, DatabaseUser user) throws Exception;
	}
	
	public static final DatabaseProtocol getByClass(Class<? extends Database> clazz) {
		for (DatabaseProtocol protocol : values()) if (protocol.constructor.equals(clazz)) return protocol;
		return null;
	}
	
	public static final DatabaseProtocol getTypeOf(Database database) {
		return getByClass(database.getClass());
	}
	
	private final Class<? extends Database> constructor;
	private final Configuration configuration;
	
	private DatabaseProtocol(Class<? extends Database> constructor, Configuration configuration) {
		this.constructor = constructor;
		this.configuration = configuration;
	}
	
	public final Class<? extends Database> getConstructor() {
		return this.constructor;
	}

	public final Database instanciateFromConfiguration(DatabaseConfiguration configuration, DatabaseUser user) throws Exception {
		return this.configuration.instanciate(configuration, user);
	}
}