package fr.lyneteam.nico.database;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.sql.Connection;

@SuppressWarnings("deprecation")
public class DatabaseMysql extends DatabaseAsynchronous {
	public static final Properties PROPERTIES;
	
	static {
		Properties properties = new Properties();
		properties.setProperty("useUnicode", String.valueOf(true));
		properties.setProperty("useJDBCCompliantTimezoneShift", String.valueOf(true));
		properties.setProperty("useLegacyDatetimeCode", String.valueOf(false));
		properties.setProperty("serverTimezone", "UTC");
		PROPERTIES = properties;
	}
	
	private final String address, database;
	private final Properties properties;
	private final int port;
	private Connection connection;
	private Statement statement;
	private int fail;
	
	public DatabaseMysql(DatabaseUser user, final String address, final int port, final String database, final String username, final String password, final boolean indexer, boolean synchronous, Properties properties) throws Exception {
		super(indexer, synchronous);
		if (user == null) user = this;
		this.address = address;
		this.port = port;
		this.database = database;
		properties.setProperty("user", username);
		properties.setProperty("password", password);
		this.properties = properties;
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.fail = 10;
		SQLException exception = null;
		while (this.fail > 0) try {
			this.reconnectSynchronously();
			this.fail = -1;
		} catch (SQLException temporaly) {
			exception = temporaly;
			this.fail--;
		}
		if (this.fail == 0) { 
			user.setDatabase(new DatabaseFake(user));
			throw exception;
		} else {
			user.setDatabase(this);
			this.start();
		}
	}
	
	public DatabaseMysql(DatabaseUser user, final String address, final int port, final String database, final String username, final String password, final boolean indexer, final boolean synchronous) throws Exception {
		this(user, address, port, database, username, password, indexer, synchronous, PROPERTIES);
	}
	
	public DatabaseMysql(DatabaseUser user, final String address, final int port, final String database, final String username, final String password, final boolean indexer) throws Exception {
		this(user, address, port, database, username, password, indexer, false);
	}
	
	public DatabaseMysql(DatabaseUser user, final String address, final int port, final String database, final String username, final String password) throws Exception {
		this(user, address, port, database, username, password, false);
	}
	
	@Override
	public void run() {
		while (true) {
			this.suspend();
			try {
				this.reconnectSynchronously();
			} catch (SQLException exception) {
				System.err.println("An error occured while trying to reconnect to the MySQL server.");
				exception.printStackTrace();
			}
		}
	}
	
	public void reconnectSynchronously() throws SQLException {
		this.connection = DriverManager.getConnection("jdbc:mysql://" + this.address + ":" + this.port + "/" + this.database, this.properties);
		this.statement = this.connection.createStatement();
	}
	
	public void execute(final DatabaseConnection connection) {
		connection.define(this);
		connection.start();
	}
	
	@Deprecated
	public final Statement getStatement() {
		try {
			if (this.statement.isClosed()) this.statement = this.connection.createStatement();
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			try {
				this.reconnect();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		return this.statement;
	}
	
	public boolean waitForConnection() {
		return this.statement == null;
	}
	
	public void execute(String request) throws SQLException {
		this.getStatement().execute(request);
	}

	public ResultSet executeQuery(String request) throws SQLException {
		return this.getStatement().executeQuery(request);
	}
}