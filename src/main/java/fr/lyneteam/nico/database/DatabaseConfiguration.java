package fr.lyneteam.nico.database;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import fr.lyneteam.nico.configuration.Config;

public class DatabaseConfiguration extends Config {
	private final static Map<DatabaseProtocol, DatabaseConfigurationEntry[]> verifier = new HashMap<DatabaseProtocol, DatabaseConfigurationEntry[]>() {
		private static final long serialVersionUID = 1L;

		{
			this.put(DatabaseProtocol.FAKE, new DatabaseConfigurationEntry[0]);
			this.put(DatabaseProtocol.FILE, new DatabaseConfigurationEntry[] {
				new DatabaseConfigurationEntry("file", String.valueOf("database.db")),
				new DatabaseConfigurationEntry("indexer", Boolean.valueOf(false)),
				new DatabaseConfigurationEntry("synchrounous", Boolean.valueOf(false)),
				new DatabaseConfigurationEntry("autocommit", Boolean.valueOf(true))
			});
			this.put(DatabaseProtocol.MYSQL, new DatabaseConfigurationEntry[] {
				new DatabaseConfigurationEntry("host", String.valueOf("127.0.0.1")),
				new DatabaseConfigurationEntry("port", Integer.valueOf(3306)),
				new DatabaseConfigurationEntry("database", String.valueOf("database")),
				new DatabaseConfigurationEntry("username", String.valueOf("username")),
				new DatabaseConfigurationEntry("password", String.valueOf("password")),
				new DatabaseConfigurationEntry("indexer", Boolean.valueOf(false)),
				new DatabaseConfigurationEntry("synchrounous", Boolean.valueOf(false))
			});
		}
	};
	private final DatabaseProtocol protocol;
	
	public DatabaseConfiguration(File file) throws Exception {
		super(file);
		String type = this.contains("type") ? this.getString("type") : "fake";
		DatabaseProtocol protocol = null;
		try {
			protocol = DatabaseProtocol.valueOf(type.toUpperCase());
		} catch (Exception exception) {
			protocol = DatabaseProtocol.FAKE;
			System.err.println("Warning, database protocol \"" + type + "\" not found.");
		}
		DatabaseConfigurationEntry[] entries = verifier.get(protocol);
		for (int number = 0; number < entries.length; number++) {
			DatabaseConfigurationEntry entry = entries[number];
			String key = entry.getKey();
			Object value = entry.getValue();
			if (this.contains(key)) {
				if (!((value instanceof Integer && this.isInt(key)) || (value instanceof String && this.isString(key)) || (value instanceof Boolean && this.isBoolean(key)))) {
					System.err.println("Warning, value of \"" + key + "\" is incorrect, setting up to \"" + value + "\"");
					this.set(key, value);
				}
			} else {
				System.err.println("Warning, value of \"" + key + "\" undefined, assuming it's \"" + value + "\"");
				this.set(key, value);
			}
		}
		this.protocol = protocol;
	}

	public final Database getDatabase(DatabaseUser user) throws Exception {
		return this.protocol.instanciateFromConfiguration(this, user);
	}
}