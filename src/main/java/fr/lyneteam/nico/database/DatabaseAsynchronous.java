package fr.lyneteam.nico.database;

public abstract class DatabaseAsynchronous extends Database implements DatabaseAsynchronousMethods {
	public DatabaseAsynchronous(boolean indexer, boolean synchronous) {
		super(indexer, synchronous);
	}

	public void reconnect() throws Exception {
		if (this.isSynchronous()) this.reconnectSynchronously(); else this.reconnectAsynchronously();
	}
	
	@SuppressWarnings("deprecation")
	public void reconnectAsynchronously() {
		this.resume();
	}
}