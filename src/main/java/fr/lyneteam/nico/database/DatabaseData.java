package fr.lyneteam.nico.database;

import java.util.Set;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DatabaseData {
	private final Map<Object, DatabaseData> data;
	
	public DatabaseData(final List<?> data) {
		this.data = new HashMap<Object, DatabaseData>();
		for (Object object : data)  this.data.put(object, null);
	}
	
	public DatabaseData(final Object data) {
		this.data = new HashMap<Object, DatabaseData>();
		this.data.put(data, null);
	}
	
	public DatabaseData() {
		this.data = new HashMap<Object, DatabaseData>();
	}
	
	public void addData(final String key, final String value) {
		if (this.data.containsKey(key)) this.data.get(key).addData(value); else this.data.put(key, new DatabaseData(value));
	}
	
	public void addData(final String value) {
		this.data.put(value, null);
	}
	
	public DatabaseData getData(final String key) {
		return this.data.get(key);
	}
	
	public boolean contains(final String key) {
		return this.data.containsKey(key);
	}
	
	public final String getAsString() {
		String rtn = "";
		for (final Object object : this.data.keySet()) rtn = String.valueOf(rtn) + object.toString();
		return rtn;
	}
	
	public final List<String> getAsListString() {
		final List<String> temporaly = new ArrayList<String>();
		for (final Object object : this.data.keySet()) temporaly.add(object.toString());
		return temporaly;
	}
	
	public final List<Integer> getAsListInteger() {
		final List<Integer> temporaly = new ArrayList<Integer>();
		for (final Object object : this.data.keySet()) temporaly.add(Integer.parseInt(object.toString()));
		return temporaly;
	}
	
	public final List<Float> getAsListFloat() {
		final List<Float> temporaly = new ArrayList<Float>();
		for (final Object object : this.data.keySet()) temporaly.add(Float.parseFloat(object.toString()));
		return temporaly;
	}
	
	public final List<Double> getAsListDouble() {
		final List<Double> temporaly = new ArrayList<Double>();
		for (final Object object : this.data.keySet()) temporaly.add(Double.parseDouble(object.toString()));
		return temporaly;
	}
	
	public final List<Long> getAsListLong() {
		final List<Long> temporaly = new ArrayList<Long>();
		for (final Object object : this.data.keySet()) temporaly.add(Long.parseLong(object.toString()));
		return temporaly;
	}
	
	public final List<BigInteger> getAsListBigInteger() {
		final List<BigInteger> temporaly = new ArrayList<BigInteger>();
		for (final Object object : this.data.keySet()) temporaly.add(new BigInteger(object.toString()));
		return temporaly;
	}
	
	public final List<Boolean> getAsListBoolean() {
		final List<Boolean> temporaly = new ArrayList<Boolean>();
		for (final Object object : this.data.keySet()) temporaly.add(Boolean.parseBoolean(object.toString()));
		return temporaly;
	}
	
	@Deprecated
	public final List<Byte> getAsListByte() {
		final List<Byte> temporaly = new ArrayList<Byte>();
		for (final Object object : this.data.keySet()) temporaly.add(Byte.parseByte(object.toString()));
		return temporaly;
	}
	
	public final byte[] getAsByteArray() {
		Set<Object> objects = this.data.keySet();
		final byte[] temporaly = new byte[objects.size()];
		for (int index = 0; index < temporaly.length; index++) temporaly[index] = Byte.parseByte(objects.toArray()[index].toString());
		return temporaly;
	}
	
	public final int getAsInteger() {
		return Integer.parseInt(this.getAsString());
	}
	
	public final float getAsFloat() {
		return Float.parseFloat(this.getAsString());
	}
	
	public final double getAsDouble() {
		return Double.parseDouble(this.getAsString());
	}
	
	public final long getAsLong() {
		return Long.parseLong(this.getAsString());
	}
	
	public final BigInteger getAsBigInteger() {
		return new BigInteger(this.getAsString());
	}
	
	public final boolean getAsBoolean() {
		return Boolean.valueOf(this.getAsString());
	}
	
	public final byte getAsByte() {
		return Byte.valueOf(this.getAsString());
	}
	
	public List<String> table(final int index) {
		final List<String> print = new ArrayList<String>();
		String separator = "";
		for (int number = 0; number < index; ++number) separator = String.valueOf(separator) + " ";
		for (final Object object : this.data.keySet()) {
			String line = String.valueOf(separator) + object + " {";
			if (!this.data.containsKey(object) || this.data.get(object) == null || this.data.get(object).keySet().size() == 0) line = String.valueOf(line) + " "; else {
				print.add(line);
				for (final String value : this.data.get(object).table(index + 1)) print.add(value);
				line = separator;
			}
			line = String.valueOf(line) + "}";
			print.add(line);
		}
		if (index == 0) for (final String line2 : print) System.out.println(line2);
		return print;
	}
	
	public Set<Object> keySet() {
		return this.data.keySet();
	}
	
	public boolean isList() {
		return this.data.keySet().size() > 1;
	}
	
	public void removeData(final String key) {
		this.data.remove(key);
	}
	
	public final Set<Map.Entry<Object, DatabaseData>> getDatas() {
		return this.data.entrySet();
	}

	public boolean isSimilar(DatabaseData to) {
		if (!(to.data.size() == this.data.size())) return false;
		for (Entry<Object, DatabaseData> entry : this.data.entrySet()) {
			if (to.data.containsKey(entry.getKey())) {
				DatabaseData data = to.data.get(entry.getKey());
				if (entry.getValue() == null) {
					if (!(data == null)) {
						return false;
					}
				} else {
					if (data == null) {
						return false;
					} else if (!entry.getValue().isSimilar(data)) {
						return false;
					}
				}
			} else return false;
		}
		return true;
	}
}