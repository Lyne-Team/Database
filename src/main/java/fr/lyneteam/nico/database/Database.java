package fr.lyneteam.nico.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Database extends Thread implements DatabaseUser {
	private final static int size;
	
	static {
		size = 1024;
	}
	
	public static void main(String[] arguments) {
		// Ignore
	}
	
	private final boolean indexer;
	private boolean synchronous;
	protected Database database;
	
	public Database(boolean indexer, boolean synchronous) {
		super("Database asynchronous connector thread.");
		this.indexer = indexer;
		this.synchronous = synchronous;
	}
	
	public final boolean isIndexed() {
		return this.indexer;
	}
	
	public final boolean isSynchronous() {
		return this.synchronous;
	}
	
	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}
	
	public void createTables(String... names) throws SQLException {
		String temporaly = "";
		if (this.indexer) temporaly = ",`index` int(255) NOT NULL";
		for (String name : names) this.execute("CREATE TABLE IF NOT EXISTS `" + name + "` (`id` varchar(" + size +") NOT NULL,`key` varchar(" + size + ") NOT NULL,`value` varchar(" + size + ") NOT NULL" + temporaly + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}
	
	public void setDatabase(Database database) {
		this.database = database;
	}
	
	public final Database getDatabase() {
		return this.database;
	}
	
	public abstract void execute(DatabaseConnection connection);
	public abstract void execute(String request) throws SQLException;
	public abstract ResultSet executeQuery(String request) throws SQLException;
}