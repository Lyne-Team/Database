package fr.lyneteam.nico.database;

public interface DatabaseAsynchronousMethods {
	public void reconnectSynchronously() throws Exception;
	public void reconnectAsynchronously();
}