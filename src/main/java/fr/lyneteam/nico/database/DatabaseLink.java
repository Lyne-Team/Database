package fr.lyneteam.nico.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DatabaseLink {
	private final Database database;
	private final String table;
	private final String id;
	private final DatabaseData data;
	private DatabaseData editable;
	
	public DatabaseLink(Database database, String table, String id) {
		this.database = database;
		this.id = id;
		this.table = table;
		this.data = new DatabaseData();
		this.editable = new DatabaseData();
	}
	
	public void getDatabaseData() throws SQLException {
		if (!(this.id == null)) {
			String temporaly = "";
			if (this.database.isIndexed()) temporaly = " ORDER BY `index`";
			ResultSet result = this.database.executeQuery("SELECT * FROM `" + this.table + "` WHERE `id`='" + this.id + "'" + temporaly);
			while (result.next()) {
				this.data.addData(result.getString("key"), result.getString("value"));
				this.editable.addData(result.getString("key"), result.getString("value"));
			}
			result.close();
		}
	}
	
	public void getDatabaseData(final DatabaseFallback fallback) {
		new Thread("Database asynchronous request thread.") {
			public void run() {
				try {
					DatabaseLink.this.getDatabaseData();
					fallback.success(DatabaseLink.this);
				} catch (SQLException exception) {
					fallback.error(exception);
				}
			}
		}.start();
	}
	
	public void saveDatabaseData() throws SQLException {
		if (!(this.id == null)) {
			String temporaly = "", temporaly1 = "";
			if (this.database.isIndexed()) {
				temporaly = ", `index`";
				temporaly1 = ",'0'";
			}
			final List<String> keys = new ArrayList<String>();
			for (final Object temp : this.data.keySet()) {
				final String key = (String) temp;
				// IF EXIST YET
				if (this.editable.contains(key)) {
					// TEST IF MODIFIED
					if (!this.editable.getData(key).isSimilar(this.data.getData(key))) {
						// PREV
						final DatabaseData previous = this.data.getData(key);
						// NOW
						final DatabaseData data = this.editable.getData(key);
						// IF LIST
						if (data.isList() || previous.isList()) {
							// DELETE LIST
							this.database.execute("DELETE FROM `" + this.table + "` WHERE `id`='" + this.id + "' AND `key`='" + this.escape(key) + "'");
							// MAKE LIST
							int index = 0;
							for (String entry : data.getAsListString()) {
								String temporaly2 = "";
								if (this.database.isIndexed()) temporaly2 = ",'" + index + "'";
								this.database.execute("INSERT INTO `" + this.table + "`(`id`, `key`, `value`" + temporaly + ") VALUES ('" + this.id + "','" + this.escape(key) + "','" + this.escape(entry) + "'" + temporaly2 + ")");
								index++;
							}
						} else {
							// IF NORMAL
							this.database.execute("UPDATE `" + this.table + "` SET `value`='" + this.escape(data.getAsString()) + "' WHERE `id`='" + this.id + "' AND `key`='" + this.escape(key) + "'");
						}
					}
					keys.add(key);
				} else {
					// IF NOT
					this.database.execute("DELETE FROM `" + this.table + "` WHERE `id`='" + this.id + "' AND `key`='" + this.escape(key) + "'");
				}
			}
			for (final Object temp : this.editable.keySet()) {
				final String key = (String) temp;
				if (!keys.contains(key)) {
					for (String entry : this.editable.getData(key).getAsListString()) {
						this.database.execute("INSERT INTO `" + this.table + "`(`id`, `key`, `value`" + temporaly + ") VALUES ('" + this.id + "','" + this.escape(key) + "','" + this.escape(entry) + "'" + temporaly1 + ")");
					}
				}
			}
		}
	}
	
	private final String escape(final String escape) {
		return escape.replace("'", "''");
	}
	
	public void displayData() {
		this.editable.table(0);
	}
	
	public final DatabaseData getData(final String key) {
		return this.getData(key, null);
	}
	
	public final DatabaseData getData(final String key, DatabaseData dflt) {
		if (this.editable.contains(key)) return this.editable.getData(key);
		return dflt;
	}
	
	public void removeData(final String key) {
		if (this.editable.contains(key)) this.editable.removeData(key);
	}
	
	public void setData(final String key, final String value) {
		this.removeData(key);
		this.addDataToList(key, value);
	}
	
	public void setData(final String key, final List<String> value) {
		this.removeData(key);
		for (final String line : value) this.addDataToList(key, line);
	}
	
	public final Set<Map.Entry<Object, DatabaseData>> getDatas() {
		return this.editable.getDatas();
	}
	
	public void addDataToList(final String key, final String value) {
		this.editable.addData(key, value);
	}
	
	public void removeDataFromList(final String key, final String value) {
		this.editable.getData(key).removeData(value);
	}

	public final boolean isEmpty() {
		return this.editable.getDatas().isEmpty();
	}
}