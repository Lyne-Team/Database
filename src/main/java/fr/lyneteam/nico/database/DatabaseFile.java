package fr.lyneteam.nico.database;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.File;
import java.sql.Connection;

@SuppressWarnings("deprecation")
public class DatabaseFile extends DatabaseAsynchronous {
	private final File file;
	private final boolean autocommit;
	private Connection connection;
	private Statement statement;
	
	public DatabaseFile(DatabaseUser user, final File file, final boolean indexer, final boolean synchrounous, final boolean autocommit) throws Exception {
		super(indexer, synchrounous);
		if (user == null) user = this;
		this.file = file;
		this.autocommit = autocommit;
		Class.forName("org.sqlite.JDBC");
		try {
			this.reconnectSynchronously();
	        user.setDatabase(this);
	        this.start();
		} catch (Exception exception) {
			user.setDatabase(new DatabaseFake(user));
		}
	}
	
	public DatabaseFile(DatabaseUser user, final File file, final boolean indexer, final boolean synchrounous) throws Exception {
		this(user, file, indexer, synchrounous, true);
	}
	
	public DatabaseFile(DatabaseUser user, final File file, final boolean indexer) throws Exception {
		this(user, file, indexer, false);
	}
	
	public void run() {
		while (true) {
			this.suspend();
			try {
				this.reconnectSynchronously();
			} catch (SQLException exception) {
				System.err.println("An error occured while trying to reconnect to the file.");
				exception.printStackTrace();
			}
		}
	}
	
	public void reconnectSynchronously() throws SQLException {
		this.connection = DriverManager.getConnection("jdbc:sqlite:" + this.file.getAbsolutePath() + "");
	    this.connection.setAutoCommit(this.autocommit);
	    this.statement = this.connection.createStatement();
	}
	
	public void execute(final DatabaseConnection connection) {
		connection.define(this);
		connection.start();
	}
	
	public void commit() throws SQLException {
		this.connection.commit();
	}
	
	@Deprecated
	public final Statement getStatement() {
		try {
			if (this.statement.isClosed()) this.statement = this.connection.createStatement();
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			try {
				this.reconnect();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		return this.statement;
	}
	
	public boolean waitForConnection() {
		return this.statement == null;
	}
	
	public void execute(String request) throws SQLException {
		this.getStatement().execute(request);
	}

	public ResultSet executeQuery(String request) throws SQLException {
		return this.getStatement().executeQuery(request);
	}
}