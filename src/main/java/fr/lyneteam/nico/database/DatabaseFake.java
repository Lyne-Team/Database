package fr.lyneteam.nico.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseFake extends Database {
	private final static SQLException exception = new SQLException("DatabaseFake isn't an database.");
	
	public DatabaseFake(DatabaseUser user) {
		super(false, false);
		if (user == null) user = this;
		user.setDatabase(this);
	}
	
	public void execute(DatabaseConnection connection) {
		try {
			throw exception;
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	public void execute(String request) throws SQLException {
		throw exception;
	}

	public ResultSet executeQuery(String request) throws SQLException {
		throw exception;
	}
}