package fr.lyneteam.nico.database;

public class DatabaseConnection extends Thread {
	private Database database;
	
	protected void define(Database database) {
		this.database = database;
	}
	
	protected final Database getDatabase() {
		return this.database;
	}
}