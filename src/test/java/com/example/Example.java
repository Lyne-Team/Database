package com.example;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.lyneteam.nico.database.DatabaseMysql;

public class Example {
	public static void main(String[] arguments) throws Exception {
		DatabaseMysql database = new DatabaseMysql(null, "database.example.com", 3306, "username", "password", "mydatabase");
		
		database.createTables("accounts", "othertable", "another");

		DatabaseLink account = new DatabaseLink(database, "accounts", "1");
		account.getDatabaseData();
		account.setData("firstname", "Foo");
		account.setData("lastname", "Bar");
		account.saveDatabaseData();
		
		account = new DatabaseLink(database, "accounts", "2");
		account.getDatabaseData();
		
		if (account.isEmpty()) {
		    account.setData("firstname", "Jean");
		    account.setData("lastname", "Testing");
		    account.setData("password", "Passw0rd");
		    account.setData("messages", "Hello world !");
		}
		
		if (account.getData("messages").getAsListString().contains("Hello world !")) {    
		    account.addDataToList("messages", "Hello jean !");
		} else {
		    account.removeData("messages");
		}
		
		account.removeDataFromList("messages", "I don't like you.");
		
		account.displayData();
	}
}