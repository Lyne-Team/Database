<?php

include_once('database/Database.php');

$database = new DatabaseMysql(null, 'database.example.com', 3306, 'username', 'password', 'mydatabase');

$database->createTables('accounts', 'othertable', 'another');

$account = new DatabaseLink($database, 'accounts', '1');
$account->getDatabaseData();
$account->setData('firstname', 'Foo');
$account->setData('lastname', 'Bar');
$account->saveDatabaseData();

$account = new DatabaseLink($database, 'accounts', '2');
$account->getDatabaseData();

if ($account->isEmpty()) {
    $account->setData('firstname', 'Jean');
    $account->setData('lastname', 'Testing');
    $account->setData('password', 'Passw0rd');
    $account->setData('messages', 'Hello world !');
}

if ($account->getData('messages')->getAsString() == 'Hello world !') {    
    $account->addDataToList('messages', 'Hello jean !');
} else {
    $account->removeData('messages');
}

$account->removeDataFromList('messages, '"I don't like you.");

$account->displayData();
