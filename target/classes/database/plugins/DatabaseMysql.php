<?php

class DatabaseMysql extends Database {
	private $address;
	private $port;
	private $database;
	private $username;
	private $password;
	private $statement;
	
	public function __construct($user, $address, $port, $database, $username, $password, $indexer = false) {
		parent::__construct($indexer);
		if ($user == null) {
			$user = $this;
		}
		$this->address = $address;
		$this->port = $port;
		$this->database = $database;
		$this->username = $username;
		$this->password = $password;			
		try {
			$this->connect();
			$user->setDatabase($this);
		} catch (Exception $exception) {
			throw $exception;
		}
	}
	
	public function reconnect() {
		$this->connect();
	}
	
	public function connect() {
		try {     
			$this->statement = new PDO('mysql:host='.$this->address.';port='.$this->port.';dbname='.$this->database.';charset=UTF8', $this->username, $this->password);
		} catch (PDOException $exception) {
			echo "new PDO('mysql:host='$this->address';port='$this->port';dbname='$this->database';charset=UTF8', $this->username, $this->password);";
			throw $exception;
		}
	}
	
	public function getStatement() {
		return $this->statement;
	}
	
	public function executePDO($request) {
		return $this->statement->query($request);
	}
	
	public function execute($request) {
		$this->executePDO($request);
	}

	public function executeQuery($request) {
		$array = array();
		$responce = $this->executePDO($request);
		foreach ($responce as $key => $object) {
			$key = $object[1];
			$value = $object[2];
			if (isset($array[$key])) {
				array_push($array[$key], $value);
			} else {
				$array[$key] = array($value);
			}
		}
		return $array;
	}
}