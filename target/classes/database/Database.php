<?php

interface DatabaseUser {
	public function setDatabase($database);
}

interface DatabaseMethods {
	public function execute($request);
	public function executeQuery($request);
}

interface DatabaseFallback {
	public function success($link);
	public function error($exception);
}

abstract class Database implements DatabaseUser, DatabaseMethods {
	const size = 1024;
	private $indexer;
	protected $instance;
	
	public function __construct($indexer) {
		$this->indexer = $indexer;
	}

	public function createTables() {
		$temporaly = "";
		if ($this->indexer) {
			$temporaly = ",`index` int(255) NOT NULL";
		}
		foreach (func_get_args() as $name) {
			$this->execute("CREATE TABLE IF NOT EXISTS `".$name."` (`id` varchar(".Database::size.") NOT NULL,`key` varchar(".Database::size.") NOT NULL,`value` varchar(".Database::size.") NOT NULL".$temporaly.") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		}
	}
	
	public function setDatabase($database) {
		$this->instance = $database;
	}
	
	public function getDatabase() {
		return $this->instance;
	}
	
	public function isIndexed() {
		return $this->indexer;
	}
}

class DatabaseData {
	private $data;
	
	public function __construct($data = null) {
		if ($data == null) {
			$this->data = array();
		} else if (is_array($data)) {
			$this->data = array();
			foreach ($data as $value) {
				$this->data[$value] = null;
			}
		} else {
			$this->data = array($data => null);
		}
	}
	
	public function addData() {
		$arguments = func_get_args();
		if (count($arguments) == 1) {
			$this->data[$arguments[0]] = null;
		} else if (count($arguments) > 1) {
			$key = $arguments[0];
			$value = $arguments[1];
			if (isset($this->data[$key])) {
				$this->data[$key]->addData($value);
			} else {
				$this->data[$key] = new DatabaseData($value);
			}
		}
	}
	
	public function getData($key) {
		$arguments = func_get_args();
	    $value = $this->data[$key];
	    if ((!(isset($value)) || $value == null) && count($arguments) > 1) {
	        return $arguments[1];
	    }
	    return $value;
	}
	
	public function contains($key) {
		foreach ($this->data as $temporaly => $value) {
			if ($key == $temporaly) {
				return true;
			}
		}
		return false;
	}
	
	public function getAsString() {
		$temporaly = "";
		foreach ($this->data as $object => $value) {
			$temporaly = $temporaly.strval($object);
		}
		return $temporaly;
	}
	
	public function getAsListString() {
		$temporaly = array();
		foreach ($this->data as $object => $value) {
			array_push($temporaly, strval($object));
		}
		return $temporaly;
	}
	
	public function getAsListInteger() {
		$temporaly = array();
		foreach ($this->data as $object => $value) {
			array_push($temporaly, intval($object));
		}
		return $temporaly;
	}
	
	public function getAsListFloat() {
		$temporaly = array();
		foreach ($this->data as $object => $value) {
			array_push($temporaly, floatval($object));
		}
		return $temporaly;
	}
	
	public function getAsListDouble() {
		$temporaly = array();
		foreach ($this->data as $object => $value) {
			array_push($temporaly, doubleval($object));
		}
		return $temporaly;
	}
	
	public function getAsListBoolean() {
		$temporaly = array();
		foreach ($this->data as $object => $value) {
			array_push($temporaly, strval($object) == 'true');
		}
		return $temporaly;
	}

	public function getAsInteger() {
		return intval($this->getAsString());
	}
	
	public function getAsFloat() {
		return floatval($this->getAsString());
	}
	
	public function getAsDouble() {
		return doubleval($this->getAsString());
	}

	public function getAsBoolean() {
		return ($this->getAsString() == 'true');
	}
	
	public function keySet() {
		$temporaly = array();
		foreach ($this->data as $key => $value) {
			array_push($temporaly, $key);
		}
		return $temporaly;
	}
	
	public function isList() {
		return count($this->data) > 1;
	}
	
	public function removeData() {
		foreach (func_get_args() as $key) {
			unset($this->data[$key]);
		}
	}
	
	public function getDatas() {
		return $this->data;
	}

	public function isSimilar($to) {
		if (!(count($to->data) == count($this->data))) {
			return false;
		}
		foreach ($this->data as $key => $value) {
			if ($to->contains($key)) {
				$data = $to->data[$key];
				if ($value == null) {
					if (!($data == null)) {
						return false;
					}
				} else {
					if ($data == null) {
						return false;
					} else if ($value->isSimilar($data)) {
						return false;
					}
				}
			} else {
				return false;
			}
		}
		return true;
	}
}

class DatabaseLink {
	private $database;
	private $table;
	private $id;
	private $data;
	private $editable;
	
	public function __construct($database, $table, $id) {
		$this->database = $database;
		$this->id = $id;
		$this->table = $table;
		$this->data = new DatabaseData();
		$this->editable = new DatabaseData();
	}
	
	public function getDatabaseData() {
		if (!($this->id == null))  {
			$temporaly = "";
			if ($this->database->isIndexed()) {
				$temporaly = " ORDER BY `index`";
			}
			$result = $this->database->executeQuery("SELECT * FROM `".$this->table."` WHERE `id`='".$this->id."'".$temporaly);
			foreach ($result as $key => $array) {
				foreach ($array as $value) {
					$this->data->addData($key, $value);
					$this->editable->addData($key, $value);
				}
			}
		}
	}

	public function saveDatabaseData() {
		if (!($this->id == null))  {
			$temporaly = "";
			$temporaly1 = "";
			if ($this->database->isIndexed()) {
				$temporaly = ", `index`";
				$temporaly1 = ",'0'";
			}
			$keys = array();
			foreach ($this->data->keySet() as $key) {
				if ($this->editable->contains($key)) {
					if (!$this->editable->getData($key)->isSimilar($this->data->getData($key))) {
						$previous = $this->data->getData($key);
						$data = $this->editable->getData($key);
						if ($data->isList() || $previous->isList()) {
							$this->database->execute("DELETE FROM `".$this->table."` WHERE `id`='".$this->id."' AND `key`='".$this->escape($key)."'");
							$index = 0;
							foreach ($data->getAsListString() as $entry) {
								$temporaly2 = "";
								if ($this->database->isIndexed()) {
									$temporaly2 = ",'".index."'";
								}
								$this->database->execute("INSERT INTO `".$this->table."`(`id`, `key`, `value`".$temporaly.") VALUES ('".$this->id."','".$this->escape($key)."','".$this->escape($entry)."'".$temporaly2.")");
								$index = $index + 1;
							}
						} else {
							$this->database->execute("UPDATE `".$this->table."` SET `value`='".$this->escape($data->getAsString())."' WHERE `id`='".$this->id."' AND `key`='".$this->escape($key)."'");
						}
					}
					array_push($keys, $key);
				} else {
					$this->database->execute("DELETE FROM `".$this->table."` WHERE `id`='".$this->id."' AND `key`='".$this->escape($key)."'");
				}
			}
			foreach ($this->editable->keySet() as $key) {
				if (!in_array($key, $keys)) {
					foreach ($this->editable->getData($key)->getAsListString() as $entry) {
						$this->database->execute("INSERT INTO `".$this->table."`(`id`, `key`, `value`".$temporaly.") VALUES ('".$this->id."','".$this->escape($key)."','".$this->escape($entry)."'".$temporaly1.")");
					}
				}
			}
		}
	}
	
	private function escape($escape) {
		return str_replace("'", "''", $escape);
	}
	
	protected function setID($id) {
		$this->id = $id;
	}
	
	protected function getID() {
		return $this->id;
	}
	
	public function isEmpty() {
		return (count($this->editable->getDatas()) == 0);
	}
	
	public function getData($key, $dflt = null) {
		if ($this->editable->contains($key)) {
			return $this->editable->getData($key);
		}
		return $dflt;
	}
	
	public function removeData($key) {
		if ($this->editable->contains($key)) {
			$this->editable->removeData($key);
		}
	}
	
	public function setData($key) {
		$this->removeData($key);
		$arguments = func_get_args();
		if (count($arguments) > 1) {
			for ($number = 1; $number < count($arguments); $number++) {
				$this->addDataToList($key, $arguments[$number]);
			}
		}
	}
	
	public function getDatas() {
		return $this->editable->getDatas();
	}
	
	public function addDataToList($key, $value) {
		$this->editable->addData($key, $value);
	}
	
	public function removeDataFromList($key, $value) {
		$this->editable->getData($key).removeData($value);
	}
	
	public function displayData() {
		var_dump($this->getDatas());
	}
}

foreach(glob(dirname(__FILE__).'/plugins/*.{php}', GLOB_BRACE) as $file) {
	include_once($file);
}